import os
import pytest
from appium import webdriver
from resources import config

PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)

def get_driver_android(activity, platform, version):
        desired_caps = {}
        desired_caps['platformName'] = platform
        desired_caps['platformVersion'] = version
        desired_caps['deviceName'] = config.DEVICE_NAME
        desired_caps['app'] = PATH(config.PATH_TO_APK)
        desired_caps['appPackage'] = config.APP_PACKAGE
        # desired_caps['appActivity'] = config.APP_PACKAGE + '.activity.' + activity
        desired_caps['appActivity'] = config.APP_PACKAGE + '.' + activity
        driver = webdriver.Remote(config.APPIUM_HOST + ':' +
                                  config.APPIUM_PORT +
                                  config.APPIUM_PATH, desired_caps)
        return driver

# This is an example of setting driver for IOS!
def get_driver_ios(activity, platform, version):
        # set up appium
        app = os.path.join(os.path.dirname(__file__),
                           '../../apps/UICatalog/build/Release-iphonesimulator',
                           'UICatalog.app')
        app = os.path.abspath(app)
        driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4723/wd/hub',
            desired_capabilities={
                'app': app,
                'platformName': platform,
                'platformVersion': version,
                'deviceName': 'iPhone Simulator'
            })
        return driver