import os
import sys

sys.path.append(os.path.dirname(__file__) + '/../')
import pytest
from time import sleep
from appium.webdriver.common.touch_action import TouchAction
from utils import functions
from resources import locators

class Screen:

    def __init__(self, activity, platform, version):
        if platform == 'ANDROID':
            self.driver = functions.get_driver_android(activity, platform,
                                                       version)
        else:
            self.driver = functions.get_driver_ios(activity, platform, version)
        self.driver.implicitly_wait(6)

    def push_start_button(self):
        start_button = self.find_element(locators.start_btn_type,
                                         locators.start_btn_text)
        self.tap(start_button)

    def teardown_class(self):
        self.driver.quit()

    def scroll(self, origin_el, destination_el):
        self.driver.scroll(origin_el, destination_el)

    def swipe(self, start_x, start_y, end_x, end_y, duration=None):
        self.driver.swipe(start_x, start_y, end_x, end_y, duration)

    def tap(self, element):
        element.click()

    # def tap(self, positions, duration=None):
    #     self.driver.tap(positions, duration)

    def flick(self,start_x, start_y, end_x, end_y):
        self.driver.flick(start_x, start_y, end_x, end_y)

    def pinch(self, element=None, percent=200, steps=50):
        self.driver.pinch(element, percent, steps)

    def zoom(self, element=None, percent=200, steps=50):
        self.driver.zoom(element, percent, steps)

    def lock(self, seconds):
        self.driver.lock(seconds)

    def shake(self):
        self.driver.shake()

    def find_element(self, by, name):
        if by == 'id':
            return self.driver.find_element_by_id(name)
        elif by == 'class':
            return self.driver.find_element_by_class_name(name)
        elif by == 'xpath':
            return self.driver.find_element_by_xpath(name)
        elif by == 'text':
            return self.driver.find_element_by_link_text(name)

    def find_elements(self, by, name):
        if by == 'id':
            return self.driver.find_elements_by_id(name)
        elif by == 'class':
            return self.driver.find_elements_by_class_name(name)
        elif by == 'xpath':
            return self.driver.find_elements_by_xpath(name)
        elif by == 'text':
            return self.driver.find_elements_by_link_text(name)

    def sleep(self, seconds):
        sleep(seconds)

    def quit(self):
        self.driver.quit()